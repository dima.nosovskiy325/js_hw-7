/* 
 Теоретичні питання
 1. Як можна створити рядок у JavaScript?
 2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?
 3. Як перевірити, чи два рядки рівні між собою?
 4. Що повертає Date.now()?
 5. Чим відрізняється Date.now() від new Date()?

Практичні завдання 
1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом 
(читається однаково зліва направо і справа наліво), або false в іншому випадку.

2. Створіть функцію, яка  перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший. Ця функція стане в нагоді для валідації форми. Приклади використання функції:
// Рядок коротше 20 символів
funcName('checked string', 20); // true
// Довжина рядка дорівнює 18 символів
funcName('checked string', 10); // false

3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через prompt. Функція повина повертати значення повних років на дату виклику функцію.
*/


/* 

Теория

1) Строку можно создать несколькими способами, например присвоив переменной через одинарные кавычки age = ''; / двойные  age = ""; / Шаблонный литерал (обратные кавычки) age = ``; / строку можно создать через глобальный обьект new String()

2) Разницы между одинарными '' и двойными "" нет никакой, в обратных кавычках можно обращаться к выражениям внутри
3) Например строки можно перебрать посимвольно и таким образом сравнить их друг с другом
4) Возвращает текущее время в милисекундах от января 70го года
5) Data.now возвращает текущее время в милисекундах от 1970 года / new Data() вызов объекта даты в который можно передавать аргументы 

*/

// Практика 

// 1 

function isPalindrome(str) {
    str = str.replace(/\s+/g, '').toLowerCase();
    let reversedStr = str.split('').reverse().join('');

    return str === reversedStr;
}

console.log(isPalindrome('А роза упала на лапу Азора'));

// 2 

function checkLength(str, maxLength) {
    return str.length <= maxLength;
     
}

console.log(checkLength('Как разрабочик програмист', 20));


// 2

function getFullAge(birthDate) {
    
    let today = new Date();
    
    let birthYear = birthDate.getFullYear();
    let birthMonth = birthDate.getMonth();
    let birthDay = birthDate.getDate();
    
    let currentYear = today.getFullYear();
    let currentMonth = today.getMonth();
    let currentDay = today.getDate();
    
    let age = currentYear - birthYear;
    
    if (currentMonth < birthMonth || (currentMonth === birthMonth && currentDay < birthDay)) {
        age--;
    }
    
    return age;
}

let userBirthDate = prompt("Введіть дату народження у форматі YYYY-MM-DD:");
let birthDate = new Date(userBirthDate);

if (!isNaN(birthDate.getTime())) {
    console.log("Кількість повних років: " + getFullAge(birthDate));
} else {
    console.log("Некоректна дата. Будь ласка, введіть дату у форматі YYYY-MM-DD.");
}